const express = require('express');
const redis = require("redis");

const app = express();
const redisPub = redis.createClient();
const redisSub = redis.createClient();

app.get('/quest', function (req, res) {
  res.send('Hello World!');

});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});

redisSub.on("message", function (channel, message) {
  //console.log(channel, message)    
});

redisSub.subscribe('userActions')