const WebSocket = require('uws');
const redis = require("redis");
 
const wss = new WebSocket.Server({ port: 8080 });

const redisPub = redis.createClient();
const redisSub = redis.createClient();
 
wss.broadcast = function broadcast(data) {
  wss.clients.forEach(function each(client) {
    if (client.readyState === WebSocket.OPEN) {
      client.send(data);
    }
  });
};
 
wss.on('connection', function connection(ws) {
  console.log('client connected', wss.clients.length || wss.clients.size)
  ws.on('message', function incoming(data) {
    redisPub.publish("userActions", data);
  });
});

wss.on('close', function close() {
  console.log('client disconnected', wss.clients.length);
});

redisSub.on("message", function (channel, message) {
  // console.log("sub channel " + channel + ": " + message);
  // msg_count += 1;
  // if (msg_count === 3) {
  //     sub.unsubscribe();
  //     sub.quit();
  //     pub.quit();
  // }
  wss.broadcast(message)
});

redisSub.subscribe('tasks')